package com.zj.ctrl;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CameraCtrl {

	private String ip;
	private int port;
	private String op;
}
